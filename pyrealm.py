#!/usr/bin/env python
"""Interactive Shell Game for learning to code with Python
By Jeremy Plack, 2015-11-22
"""
from stage_one import start_journey
import os
import json
import requests
import argparse
parser = argparse.ArgumentParser()

def main():
    return


if __name__ == "__main__":
    os.system('cls' if os.name == 'nt' else 'clear')
    ans=True
    while ans:
        print ("""

        ***************************************************
        ***************************************************
          ____        ____            _
         |  _ \ _   _|  _ \ ___  __ _| |_ __ ___
         | |_) | | | | |_) / _ \/ _` | | '_ ` _  |
         |  __/| |_| |  _ <  __/ (_| | | | | | | |
         |_|    \__, |_| \_\___|\__,_|_|_| |_| |_|
                |___/

        ***************************************************
        ***************************************************

        1. Start your journey
        2. Progress
        3. Exit/Quit
        """)
        ans=raw_input("What would you like to do? ")
        if ans=="1":
            start_journey()
        elif ans=="2":
            show_progress()
        elif ans=="3":
            print("\n Goodbye")
            break
        elif ans !="":
            print("\n Not Valid Choice Try again")

if __name__ == "__main__":
    import doctest
    doctest.testmod()
