# Pyrealm

An interactive shell game that teaches the basics of programming in Python.

## How to use

1. clone this repo `git clone thisrepo`
2. cd into the repo directory `cd thisrepo`
3. run the game `python pyrealm.py`

## Prerequisites

#### Python >= 2.7+ < 3.0
You will need to have Python 2.7 installed on your computer. if you already have Python 3.0+, then you can use virtualenv, to create a 2.7 environment that won't interfere with your 3.0+ system environment.
