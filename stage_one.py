import time
import os
import re
import six

def start_journey():
    os.system('cls' if os.name == 'nt' else 'clear')
    print """
-------------------
Welcome to Pyrealm, a humble village in need of a hero.
This is where your journey begins..
    """
    time.sleep(0.75)
    print """
It's late when you arrive in the village of Pyrealm.
You're roaming the pathways searching for a place to stay when
you are approached by a shadowy figure, its a man, wearing a large,
dark robe, you can't make out his features.
    """
    raw_input("Continue... (press enter)")
    print """
-------------------

Strange Old Man: "I don't recall seeing you around this village.
You look like a brave and powerful warrior, just the kind of
guy we need around here. You see, Pyrealm was once a beautiful
and prosperous village, but we have been under attack by a family of Dragons
for decades, and with weakened defenses we have suffered a string
barbarian seiges. New leadership was imposed to slay the Dragons, combat the
barbarians and bring jobs and villagers back to the village."
    """
    raw_input("Continue... (press enter)")
    print """
"Years of mismanagement has only worsened the situation, our defenses are
non-existent, our infrastructure is crumbling, trade has ground to a halt and
starving villagers are leaving in droves."

Are you willing to accept the challenge to restore the village of Pyrealm
to her former glory?
    """
    accept_challenge = raw_input('(y/n)')
    if accept_challenge == 'y' or 'Y':
        print """
"Excellent! I have faith in you young warrior, you are our only hope!
Tell me, what is your name?"

-------------------
To tell the old man your name, assign the string
'whatever you want your name to be'
to the variable player.
"""
        evaluating = enter_name()
        while evaluating:
            enter_name()
    else:
        print """
"Please help us, our village needs you and I believe you can do this.
You shall be rewarded with a bounty of coding skills should you succeed."

"Please return when you are ready to accept the challenge."
        """
        time.sleep(5)
        os.system('cls' if os.name == 'nt' else 'clear')
        return

def challenge_one_complete(player):
    message = """
-------------------
You've just used the Python syntax for variable assignment.
entering player = '%s', stores the string value %s in a variable
in the computer's memory that is callable by the name player.
-------------------
    """
    print message % (player,player)
    challenge_two(player)

def challenge_two(player):
    print """
Strange Old Man: "Pleasure to make your aquaintance.. uh.. what was your name
again?"

In Python the print statement is used to display information in the terminal,
use a print statement and call the variable player to tell the forgetful old man
your name again.
    """
    evaluating_two = print_name(player)
    while evaluating_two:
        print_name(player)


def enter_name():
    end_h = False
    player_input = raw_input()
    if player_input == 'h' or player_input == 'H' or player_input == 'hint' or player_input == 'help':
        print "Hint: use the = (equals) sign to assign a value to a variable."
        end_h = True
        return(end_h)
    else:
        try:
            exec(player_input)
        except Exception as e:
            print "ERROR:"
            print e

        if 'player' in locals():
            if isinstance(player, six.string_types):
                if re.match('^([a-zA-Z])*$', player):
                    end_h = False
                    challenge_one_complete(player)
                else:
                    print "thats a strange name, try entering only alphabetic characters."
                    end_h = True
            else:
                print "player isn't a string."
                end_h = True
        else:
            print """
player variable isn't defined. In Python a string variable is defined like this:
variable_name = 'variable value'
            """
            end_h = True
        return(end_h)

def stage_one_complete():
    os.system('cls' if os.name == 'nt' else 'clear')
    print """
Well done. you have completed your first stage of your, journey!

So far you have learned about variables, how to assign them a string type value
and how to call variables.
You used a print statement to display the value of your newly created variable,
you can call variables to inject their values elsewhere in your code,
you can also reassign a new value to your variable.

String is one type of data you can assign to a variable, it means a string of
characters, and is always wrapped in qoutes ('' single, "" or double).
You'll soon learn about other types of data as well.
    """

def print_name(player):
    end_h = False
    your_name = raw_input()
    try:
        exec(your_name)
    except Exception as e:
        print "ERROR:"
        print e

    if your_name == 'print player':
        message = """
Strange Old Man: "oh yes, %s. I'm getting forgetful in my old age."

-------------------
You've just called the variable player that you created eariler inside a print
statement, in Python this will cause the value that value that you gave player
to be "printed" in the console.
-------------------
        """
        print message % (player)
        raw_input("Continue... (press enter)")
        end_h = False
        stage_one_complete()

    else:
        print """
You need to use the print statement with the variable player.
Make sure NOT to put quotes around 'player', this would pass the string literal
p-l-a-y-e-r to the interpreter, not the string value of the variable named player.
        """
        end_h = True
        return(end_h)
    return(end_h)
